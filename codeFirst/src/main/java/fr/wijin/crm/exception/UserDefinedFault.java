
package fr.wijin.crm.exception;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDefinedFault", propOrder = { "message" })
public class UserDefinedFault {

	protected String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String value) {
		this.message = value;
	}

}
