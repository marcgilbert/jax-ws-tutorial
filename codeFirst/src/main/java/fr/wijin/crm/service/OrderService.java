package fr.wijin.crm.service;

import fr.wijin.crm.exception.UserDefinedException;
import fr.wijin.crm.model.Order;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;

@WebService
public interface OrderService {

	@WebMethod
	public Order order(@WebParam(name = "id") String id) throws UserDefinedException;

}
