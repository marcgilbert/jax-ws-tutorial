package fr.wijin.crm.app;

import fr.wijin.crm.service.impl.OrderServiceImpl;
import jakarta.xml.ws.Endpoint;

public class ServerApp {
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:9980/orderService", new OrderServiceImpl());
		System.out.println("orderService Started!");
	}
}
