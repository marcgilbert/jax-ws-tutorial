
package fr.wijin.crm.service.user.generated;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.wijin.crm.service.user.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserDefinedFault_QNAME = new QName("http://wijin/crm/soap", "UserDefinedFault");
    private final static QName _UserIdList_QNAME = new QName("http://wijin/crm/soap", "UserIdList");
    private final static QName _UserInfoList_QNAME = new QName("http://wijin/crm/soap", "UserInfoList");
    private final static QName _CreateUser_QNAME = new QName("http://wijin/crm/soap", "CreateUser");
    private final static QName _CreatedUserId_QNAME = new QName("http://wijin/crm/soap", "CreatedUserId");
    private final static QName _UserDataGrants_QNAME = new QName("http://wijin/crm/soap", "grants");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.wijin.crm.service.user.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserDefinedFault }
     * 
     */
    public UserDefinedFault createUserDefinedFault() {
        return new UserDefinedFault();
    }

    /**
     * Create an instance of {@link UserIdWrapper }
     * 
     */
    public UserIdWrapper createUserIdWrapper() {
        return new UserIdWrapper();
    }

    /**
     * Create an instance of {@link UserInfoWrapper }
     * 
     */
    public UserInfoWrapper createUserInfoWrapper() {
        return new UserInfoWrapper();
    }

    /**
     * Create an instance of {@link UserWrapper }
     * 
     */
    public UserWrapper createUserWrapper() {
        return new UserWrapper();
    }

    /**
     * Create an instance of {@link UserInfo }
     * 
     */
    public UserInfo createUserInfo() {
        return new UserInfo();
    }

    /**
     * Create an instance of {@link UserData }
     * 
     */
    public UserData createUserData() {
        return new UserData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDefinedFault }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserDefinedFault }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "UserDefinedFault")
    public JAXBElement<UserDefinedFault> createUserDefinedFault(UserDefinedFault value) {
        return new JAXBElement<UserDefinedFault>(_UserDefinedFault_QNAME, UserDefinedFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserIdWrapper }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserIdWrapper }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "UserIdList")
    public JAXBElement<UserIdWrapper> createUserIdList(UserIdWrapper value) {
        return new JAXBElement<UserIdWrapper>(_UserIdList_QNAME, UserIdWrapper.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserInfoWrapper }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserInfoWrapper }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "UserInfoList")
    public JAXBElement<UserInfoWrapper> createUserInfoList(UserInfoWrapper value) {
        return new JAXBElement<UserInfoWrapper>(_UserInfoList_QNAME, UserInfoWrapper.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserWrapper }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserWrapper }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "CreateUser")
    public JAXBElement<UserWrapper> createCreateUser(UserWrapper value) {
        return new JAXBElement<UserWrapper>(_CreateUser_QNAME, UserWrapper.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserIdWrapper }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserIdWrapper }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "CreatedUserId")
    public JAXBElement<UserIdWrapper> createCreatedUserId(UserIdWrapper value) {
        return new JAXBElement<UserIdWrapper>(_CreatedUserId_QNAME, UserIdWrapper.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "grants", scope = UserData.class)
    public JAXBElement<String> createUserDataGrants(String value) {
        return new JAXBElement<String>(_UserDataGrants_QNAME, String.class, UserData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://wijin/crm/soap", name = "grants", scope = UserInfo.class)
    public JAXBElement<String> createUserInfoGrants(String value) {
        return new JAXBElement<String>(_UserDataGrants_QNAME, String.class, UserInfo.class, value);
    }

}
