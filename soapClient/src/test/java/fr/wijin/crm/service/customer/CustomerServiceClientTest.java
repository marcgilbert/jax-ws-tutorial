package fr.wijin.crm.service.customer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import fr.wijin.crm.customer.client.CustomerData;
import fr.wijin.crm.customer.client.CustomerIdWrapper;
import fr.wijin.crm.customer.client.CustomerInfoWrapper;
import fr.wijin.crm.customer.client.CustomerWrapper;
import fr.wijin.crm.customer.client.UserDefinedException;

class CustomerServiceClientTest {
	
	private CustomerServiceClient monClientWS = new CustomerServiceClient(
			"http://localhost:9990/customerService?wsdl");
    @Test
    void customer_found() throws UserDefinedException {
        CustomerIdWrapper customerIdList = new CustomerIdWrapper();
        customerIdList.getCid().add("100");
        CustomerInfoWrapper ret = monClientWS.getCustomer(customerIdList);
        assertEquals(1, ret.getCustomerInfo().size());
        assertEquals("100", ret.getCustomerInfo().get(0).getCid());
    }
     
    @Test
    void customer_not_found() throws UserDefinedException {
        CustomerIdWrapper customerIdList = new  CustomerIdWrapper();
        assertThrows(UserDefinedException.class, () -> {monClientWS.getCustomer(customerIdList); });     
    }
    
    @Test
    void create_customer_ok() throws UserDefinedException {
    	CustomerWrapper customer = new CustomerWrapper();
    	CustomerData customerData = new CustomerData();
    	customerData.setFirstname("Test");
    	customer.getCustomerData().add(customerData);
    	CustomerIdWrapper ret = monClientWS.createCustomer(customer);
    	assertEquals(1, ret.getCid().size());
    	assertEquals("99", ret.getCid().get(0));
    }

}
