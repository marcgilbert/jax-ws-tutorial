package fr.wijin.crm.service.impl;

import fr.wijin.crm.exception.UserDefinedException;
import fr.wijin.crm.exception.UserDefinedFault;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.Order;
import fr.wijin.crm.service.OrderService;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;

@WebService
public class OrderServiceImpl implements OrderService {

	@WebMethod
	public Order order(@WebParam(name = "id") String id) throws UserDefinedException {
		if (null == id || "".equals(id)) {
			UserDefinedFault e = new UserDefinedFault();
			e.setMessage("Order id vide");
			throw new UserDefinedException("Merci de renseigner un identifiant", e);
		}
		return getOrder(id);

	}

	/**
	 * Récupération des orders
	 * 
	 * @param ids
	 * @return
	 */
	private Order getOrder(String id) {
		Order order = new Order();
		order.setId(Integer.valueOf(1));
		order.setLabel("Intitulé commande");
		order.setNumberOfDays(Double.valueOf(3));
		order.setAdrEt(Double.valueOf(450));
		order.setStatus("En cours");
		order.setTva(Double.valueOf(20.0));
		order.setNotes("Notes");
		order.setCustomer(buildDummyCustomer(id));
		return order;
	}

	/**
	 * Construction d'un customer
	 * 
	 * @param id l'identifiant
	 * @return
	 */
	private Customer buildDummyCustomer(String id) {
		Customer customer = new Customer();
		customer.setId(Integer.valueOf(id));
		customer.setFirstname("Firstname_" + id);
		customer.setLastname("Lastname_" + id);
		customer.setCompany("Company_" + id);
		customer.setMail("Firstname_" + id + "@test.fr");
		customer.setPhone("Phone_" + id);
		customer.setMobile("Mobile_" + id);
		customer.setActive(true);
		return customer;
	}

}
