package fr.wijin.crm.service.customer;

import java.net.MalformedURLException;
import java.net.URL;

import fr.wijin.crm.customer.client.CustomerIdWrapper;
import fr.wijin.crm.customer.client.CustomerInfoWrapper;
import fr.wijin.crm.customer.client.CustomerService;
import fr.wijin.crm.customer.client.CustomerServiceImplService;
import fr.wijin.crm.customer.client.CustomerWrapper;
import fr.wijin.crm.customer.client.UserDefinedException;
import fr.wijin.crm.customer.client.UserDefinedFault;

public class CustomerServiceClient {

	private String serviceUrl;

	public CustomerServiceClient(String serviceUrl) {
		super();
		this.serviceUrl = serviceUrl;
	}

	public CustomerInfoWrapper getCustomer(CustomerIdWrapper customerIdList) throws UserDefinedException {

		try {
			URL url = new URL(serviceUrl);
			CustomerServiceImplService empService = new CustomerServiceImplService(url);
			CustomerService eSrc = empService.getCustomerServiceImplPort();
			return eSrc.customer(customerIdList);
		} catch (MalformedURLException e) {
			UserDefinedFault fault = new UserDefinedFault();
			fault.setMessage(e.getMessage());
			throw new UserDefinedException("MalformedURLException", fault);
		}

	}

	public CustomerIdWrapper createCustomer(CustomerWrapper customer) throws UserDefinedException {
		try {
			URL url = new URL(serviceUrl);
			CustomerServiceImplService empService = new CustomerServiceImplService(url);
			CustomerService eSrc = empService.getCustomerServiceImplPort();
			return eSrc.createCustomer(customer);
		} catch (MalformedURLException e) {
			UserDefinedFault fault = new UserDefinedFault();
			fault.setMessage(e.getMessage());
			throw new UserDefinedException("MalformedURLException", fault);
		}
	}

}
