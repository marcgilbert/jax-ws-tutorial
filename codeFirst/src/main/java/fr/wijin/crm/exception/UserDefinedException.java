
package fr.wijin.crm.exception;

import jakarta.xml.ws.WebFault;

@WebFault(name = "UserDefinedFault", targetNamespace = "http://wijin/crm/")
public class UserDefinedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8520022815385110532L;

	private UserDefinedFault faultInfo;

	/**
	 * 
	 * @param faultInfo
	 * @param message
	 */
	public UserDefinedException(String message, UserDefinedFault faultInfo) {
		super(message);
		this.faultInfo = faultInfo;
	}

	/**
	 * 
	 * @param faultInfo
	 * @param cause
	 * @param message
	 */
	public UserDefinedException(String message, UserDefinedFault faultInfo, Throwable cause) {
		super(message, cause);
		this.faultInfo = faultInfo;
	}

	/**
	 * 
	 * @return returns fault bean: UserDefinedFault
	 */
	public UserDefinedFault getFaultInfo() {
		return faultInfo;
	}

}
