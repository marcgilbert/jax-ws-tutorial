package fr.wijin.soap.service.customer.impl;

import jakarta.xml.ws.Endpoint;

public class ServerApp {
	public static void main(String[] args) {
        Endpoint.publish("http://localhost:9990/customerService", new CustomerServiceImpl());
        System.out.println("customerService Started!");
    }
}
